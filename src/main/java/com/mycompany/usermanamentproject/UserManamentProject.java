/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.usermanamentproject;

import java.util.ArrayList;

/**
 *
 * @author offxcbo
 */
public class UserManamentProject {

    public static void main(String[] args) {
        User admin = new User("admin","Administrator","pass@1234",'M','A');
        User user1 = new User("user1","user1","pass@1234",'M','U');
        User user2 = new User("user2","user2","pass@1234",'F','U');
        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        User[] userArray = new User[3];
        userArray[0] = admin;
        userArray[1] = user1;
        userArray[2] = user2;
        
        for(int i =0 ; i< userArray.length;i++){
            System.out.println(userArray[i]);
        }
        
        ArrayList<User> userList = new ArrayList<User>();
        System.out.println("ArrayList");
        userList.add(admin);
        System.out.println(userList.get(0)+"list size"+ userList.size());
        userList.add( user1);
       System.out.println(userList.get(1)+"list size"+ userList.size());
        userList.add( user2);
       System.out.println(userList.get(2)+"list size"+ userList.size());
    }
}
